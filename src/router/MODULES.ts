export default {
  Home: 'Home',
  TabNav: 'TabNav',
  Recent: 'Recent',
  AddressBook: 'AddressBook',
  Profile: 'Profile',
  Login: 'Login',
  Register: 'Register',
  Chat: 'Chat',
  Search: 'Search',
  FriendRequest: 'FriendRequest',
  ApplyToFriend: 'ApplyToFriend',
  AddFriend: 'AddFriend',
};
